---
title: Blog nr.14
subtitle: woensdag
date: 2018-03-21
categories: ["Design Challenge 3"]
---

Voorbereiden Creatieve Brainstormsessie
<!--more-->


## Voorbereiden Creatieve Brainstormsessie
Vandaag heb ik samen met Gabi op school de creatieve brainstormsessie voorbereid. Dit hebben we gedaan door een uitgebreid ‘plan van aanpak’ op te stellen. Hierin hebben we de door ons gekozen technieken uitgelegd, alle details van de sessie vastgelegd etc. De samenwerking verliep erg soepel. Ondanks de nodige afleiding op de gang hebben we stap voor stap gedaan wat er gedaan moest worden. We hebben beiden 2 creatieve technieken uitgekozen die ons erg bruikbaar leken. Na overleg hebben we besloten deze te gaan gebruiken. Als ‘Energizer’ hebben we gekozen voor dezelfde opdracht als die in de workshop Creatieve Technieken; ‘het elkaar al aankijkend portretteren’.

