---
title: Blog nr.09
date: 2017-12-05
categories: ["Design Challenge 2"]
---

Vandaag heb ik me beziggehouden met het testplan. Ik deze later op de dag ook gevalideerd. De validatie verliep vrij goed. Wel was er nog genoeg ruimte voor verbetering. Ik alle feedback tijdens het valideren nauwkeurig genoteerd en dit gelijk doorgespeeld naar mijn teamgenoten. Ik heb ze ook gelijk een foto van het formulier gestuurd. Ook heb ik mij beziggehouden met het maken van een ‘Low Fidelity Prototype’. Dit heb ik in het programma Sketch van Apple uitgewerkt.