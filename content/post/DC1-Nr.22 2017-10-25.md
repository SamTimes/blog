---
title: Blog nr.22
date: 2017-10-25
categories: ["Design Challenge 1"]
---

’S ochtends voor het begin van de expositie heb ik nog alle kaartjes uitgeprint in de studieruimte. Samen met Sem en Nick heb ik de kaartje uitgeknipt, gesorteerd en er nette stapeltjes van gemaakt. Hierna heb ik met mijn groepsgenoten onze stand ingericht met ons prototype en alle andere producten. Om 10 uur begon de expositie en hebben we elkaar een beetje afgewisseld zodat er altijd minimaal 2 mensen stonden om te pitchen. Nick heeft het meest regelmatig gepitcht. Het was leerzaam om alle reacties op ons spel te horen en zeker ook om te zien en te horen wat de de andere groepjes allemaal bedacht en gemaakt hadden. De expositite was succesvol. De leraren hebben ons spel zelfs tot onze grote verbazing genomineerd tot één van de besten en uitgeroepen tot een gedeelde 2e/3e plek van alle 45 spellen!