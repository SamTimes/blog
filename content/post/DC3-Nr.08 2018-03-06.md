---
title: Blog nr.08
subtitle: Dinsdag
date: 2018-03-06
categories: ["Design Challenge 3"]
---

Terugblik vakantie, Lifestyle Diary
<!--more-->


## Terugblik vakantie
In de vakantie heb ik nauwkeurig in een logboek bijgehouden wat ik eet, hoeveel ik beweeg en wat ik doe ter ontspanning. Mijn teamgenoten hebben hetzelfde gedaan. Het is nog een beetje de vraag hoe we dit onderzoek gaan gebruiken en doorvoeren in ons concept. We hebben in ieder geval elkaars resultaten besproken. Hierop volgden de nodige discussies.

## Lifestyle Diary
Mijn teamgenoten en ik zijn vandaag aan de lifestyle Diary begonnen. Omdat we eerst in de veronderstelling waren dat de Lifestyle Diary gezamenlijk (dus als team) gemaakt mocht worden hebben we veel tijd verloren vandaag. We waren druk bezig met het bedenken hoe we onze verschillende levensstijlen konden samenvoegen tot één Lifestyle Diary toen we van een docent hoorden dat het absoluut individueel gemaakt moest worden. Dit is omdat het anders niet voldoende per persoon beoordeeld kan worden.
