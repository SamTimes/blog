---
title: Blog nr.06
date: 2017-11-29
categories: ["Design Challenge 2"]
---

Vandaag ben ik dus met mijn team extra bijeengekomen. We hebben gezamenlijk de nodige knopen doorgehakt en aan de ‘Customer Journey Map’ gewerkt. Hierbij kwam flink wat brainstorming kijken! Ik heb het idee dat we vandaag als team wel wat verder zijn gekomen. Wel besefte ik me wederom dat onze ontwerpkeuzes nog grotendeels gebaseerd waren op aannames die wij zelf maakten.