---
title: Blog nr.04
subtitle: Dinsdag
date: 2018-02-13
categories: ["Design Challenge 3"]
---

Nieuwe teams samenstellen, Teaminventarisatie, Plan van aanpak
<!--more-->

## Nieuwe teams samenstellen
Vandaag hebben we klassikaal, aan de hand van 5 verschillende ‘teamrollen’ de nieuwe teams samengesteld. Iedereen moest een meest dominante rol kiezen en een rol die bij je op de tweede plek komt. Mijn belangrijkste rol is die van ‘Concepter’. Ik had ‘Visual Designer’ als tweede rol gekozen. Aangezien er weinig ‘Concepters’ waren was het al snel duidelijk dat ik deze rol moest behouden. De vijf klasgenoten die als rol ‘Teamcaptain’ hadden, moesten één voor één iemand kiezen van een nog in dat team ontbrekende rol. Ik werd door Teamcaptain Ricardio gekozen.

Teaminventarisatie
Er is duidelijk en open besproken wat eenieders sterke en zwakke punten zijn. Ook is er besproken wat onze gezamenlijke en individuele doelen zijn. Om dit alles te verduidelijken heb ik een individueel inventarisatieprofiel opgesteld van mijzelf. Hierin heb ik alle belangrijke informatie samengevat die mijn teamgenoten moeten weten tijdens het samenwerken met mij. Ik hoop dat ik hiermee de samenwerking voor een stukje bevorderd heb. We kregen de opdracht om de stad in te gaan om elkaar beter te leren kennen en om hier de inspiratie te vinden om een teamnaam te verzinnen en een teamfoto te maken. De teamnaam was snel bedacht. Ik zei ineens terwijl we langs de Markthal liepen: The Healthy Bastards. Ik zei dit meer als grap maar het viel goed in de smaak. The Healthy Bastards waren geboren. We liepen op dat moment richting de Markthal omdat we graag een teamfoto wilden die gezondheid uitstraalde. We dachten gelijk aan fruit en zijn hiernaar opzoek gegaan. Buiten de Markthal stond een bakfiets tegen die er stond als reclame. Op initiatief van teamgenote Gabi zijn vier van ons de bakfiets te gaan zitten. Teamgenoot Nick maakte de foto. Er werd gelijk besloten dat hij er samen met een hele berg fruit later in gefotoshopt zou worden. Iedereen vond het een leuk idee en dit is dezelfde dag nog gemaakt door teamgenoot Wesley

Plan van aanpak
Vervolgens zijn mijn teamgenoten en ik gezamenlijk begonnen aan het plan van aanpak. 
We hebben al een goed begin gemaakt. We hebben gezamenlijk de studioplanning doorgenomen en een nieuw bord aangemaakt de planningswebsite Trello. Vervolgens heb ik mij nog eens goed verdiept in het vraagstuk en met mijn teamgenoten besproken welke ontwerpvragen we zouden opstellen. Ricardio heeft dit toen genoteerd.
