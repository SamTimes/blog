---
title: Blog nr.18
date: 2017-10-13
categories: ["Design Challenge 1"]
---

Met mijn groepje een taakverdeling gemaakt (voornamelijk voor in de herfstvakantie). Ik ben o.a. verantwoordelijk voor het uitwerken van de spelregels en de kaartjes en het maken van de pionnen.