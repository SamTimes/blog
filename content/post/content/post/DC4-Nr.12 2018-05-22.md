---
title: Blog nr.12
subtitle: Dinsdag
date: 2018-05-22
categories: ["Design Challenge 4"]
---

Styleguide kiezen, Testplan
<!--more-->


## Styleguide kiezen
Definitieve styleguide gekozen voor de huisstijl. Mijn teamgenoten en ik hebben allemaal een eigen styleguide gemaakt. Vandaag hebben we deze gezamenlijk bekeken en besproken. We hebben gezamenlijk een logo gekozen dat we het beste vonden. Ook hebben we het lettertype gekozen dat ik in mijn styleguide had.

## Testplan
Vandaag ben ik begonnen met het testplan. Ik heb hierin beschreven hoe ik de meetbare hypotheses ga testen. Ik heb geprobeerd dit testplan specifieker op te stellen dan ik voorheen deed aan de hand van de feedback die ik kreeg op de testplannen uit OP2. Doordat er al gelijk een duidelijke manier van aanpak was, wisten mijn teamgenoten en ik wat zijn/haar taken zouden zijn tijdens het testen. Iedereen weet goed waar we achter willen komen tijdens het testen en welke informatie bruikbaar kan zijn voor onze vervolgkeuzes binnen het ontwerpproces. 
