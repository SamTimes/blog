---
title: Blog nr.13
subtitle: Dinsdag
date: 2018-05-25
categories: ["Design Challenge 4"]
---

Presentatie docenten, Testen, Testrapport opstellen
<!--more-->


## Presentatie docenten
Om 9:30 begonnen we studiodag met een presentatie door de docenten Nikki en Fedde. 

## Testen
Het was eindelijk zover. We gingen testen! Het was al met al een succesvolle dag. Niet alle mensen die we aanspraken reageerde even enthousiast, maar dit hadden we ook niet verwacht. Uiteindelijk hebben we toch veel bruikbare informatie kunnen winnen. Eén jongen was zelfs bereid om ons even binnen te laten zodat we ons lichtgevende prototype beter konden laten zien. 
Ik heb geprobeerd uit mijn comfort zone te stappen en meer de confrontatie aan te gaan met de doelgroep. 
Het bleek dat het voor velen nog onduidelijk was wat de motivatie achter het spelen van het spel precies was en hoe zij er zelf beter van zouden worden. Dit verbeterpunt heb ik uiteraard achteraf gelijk genoteerd in het testrapport en hier ben ik later mee aan de slag gegaan. 

## Testrapport opstellen
Na het testen ben ik in samenwerking met mijn teamgenoten gelijk aan het testrapport begonnen. Door de scherpe feedback van de jongeren tijdens de interviews werd ik gedwongen om tot nieuwe inzichten te komen. Hierdoor heb ik alsnog aanpassingen gemaakt aan het concept. 

