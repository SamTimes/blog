---
title: Blog nr.07
subtitle: Dinsdag
date: 2018-05-08
categories: ["Design Challenge 4"]
---

Styleguide
<!--more-->


## Styleguide
We zijn weer begonnen na de meivakantie. Ik ben vandaag begonnen met het maken van een styleguide. Hierin heb ik mijn gekozen kleurgebruik en lettertypen aangeduid en het proces laten zien van het ontwerpen van een logo. Dit laatste heb ik eraan toegevoegd na feedback op een eerder gemaakte styleguide waarin stond dat dit er ook bij zou kunnen. 
