---
title: Blog nr.05
subtitle: Dinsdag
date: 2018-04-24
categories: ["Design Challenge 4"]
---

Vervolg deskresearch, Gebruikersprofiel
<!--more-->


## Vervolg deskresearch
Deze week heb ik mijn deskresearch vervolgd. Alhoewel ik nog steeds niet het gevoel heb dat ik echt veel informatie gevonden heb die bruikbaar gaat zijn voor mijn ontwerpkeuzes, ben ik wel goed op de hoogte van veel dingen die zich afspelen in en rondom Carnisse.

## Gebruikersprofiel
Omdat het lastig was om een duidelijk beeld te krijgen van de doelgroep uit Carnisse, hebben mijn teamgenoten en ik allemaal een ‘gebruikersprofiel’ opgesteld gebaseerd op onze eigen bevindingen gemaakt door observatie en deskresearch. Deze hebben we vervolgens bij elkaar gelegd om zo een duidelijker beeld te krijgen van onze doelgroep. 
