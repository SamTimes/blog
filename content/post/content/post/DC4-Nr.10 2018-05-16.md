---
title: Blog nr.10
subtitle: Woensdag
date: 2018-05-16
categories: ["Design Challenge 4"]
---

Conceptbespreking, Workshop Empathie, Workshop Fieldresearch
<!--more-->


## Conceptbespreking
Ik heb om 11 uur met mijn teamgenoten afgesproken op school om een goed gesprek te hebben over ons concept. Omdat we er nog steeds niet helemaal uit waren hebben we besloten vandaag verder te gaan. We zijn uiteindelijk verder gekomen dan we dachten.

## Workshop Empathie
Door: Bob Joziasse
Na de uitleg van Bob moesten we in groepjes van 3 een rollenspel spelen. Om beurten was er iemand: de interviewer, observant of de geïnterviewde. Op deze manier konden we zelf gaan ervaren hoe het is om empathie te kunnen tonen en te herkennen. Ook kon je je eigen lichaamstaal afstemmen op de ander. Deze opdrachten waren erg leerzaam en leuk tegelijk.

## Workshop Fieldresearch
Door: Michael Anhalt
Bij deze workshop was ik de enige aanwezige leerling. Dit resulteerde in een zeer leerzame privéworkshop van Micha. Hij heeft op een uitgebreide en zeer interessante zijn boek “Blokmapping; enkele vierkante meters Rotterdam” met mij doorgenomen. Hierin staan letterlijk jaren en jaren aan desk- en fieldresearch in. Een bijzondere ervaring. Aan het einde mocht ik zelfs een gesigneerde versie van dit boek meenemen!

