---
title: Blog nr.11
subtitle: Vrijdag
date: 2018-05-18
categories: ["Design Challenge 4"]
---

Algemene bespreking, Feedbacksessie, Design Rationale
<!--more-->


## Algemene bespreking
Ik ben de dag begonnen met een algemene bespreking samen met mijn teamgenoten. We hebben de nodige knopen doorgehakt. Sommige ideeën die te mooi waren om waar te zijn hebben we eindelijk losgelaten. Een voorbeeld hiervan is het idee om interactieve schermen in een park te plaatsen.

## Feedbacksessie
Vandaag heb ik gezamenlijk met mijn team feedback gevraagd aan docente Nikki Huisman op ons huidige concept.

Feedback Nikki:

Vragen:
Waar hebben ze behoefte aan?

Doelgroep:
Mensen (vooral jongeren) die te weinig buiten komen

Mogelijke ideeën:
Park waar je kan ontspannen en interactieve activiteiten kunt doen
Skatepark incl muziek

Routewijzers (pijlen, tegels) laten sponsoren door bedrijven

Wat is de waarde van het concept en hoe is die waarde betekenisvol voor de doelgroep?

Persoonlijk:
Starrts: “je moet het allemaal als 1 geheel zien niet allemaal losse kerntaakjes”

Beschrijf: wat ik vorige keer niet gedaan heb, heb ik dit project wel gedaan, namelijk etc.

Bij twijfel: gewoon opsturen naar Nikki

## Design Rationale
Ook in deze periode heb ik een Design Rationale geschreven. Dankzij de workshop Empathie die ik heb gevolgd weet ik beter hoe ik mij kan inleven in een doelgroep. Ook nu was het weer een uitdaging om alles goed te onderbouwen. Ik heb goed gekeken naar wat ik kon verbeteren ten opzichte van de Design Rationale uit OP2. De belangrijkste gedachte die ik tijdens het schrijven had was dat ik alles dusdanig duidelijk op moest schrijven dat een externe persoon die nog niets af weet van mijn concept het kan lezen en begrijpen. 

Ik heb geprobeerd het uiteindelijke doel niet uit het oog te verliezen. Ik heb de Design Rationale geschreven met in mijn achterhoofd het idee dat iedereen het moet kunnen begrijpen. Ook heb ik geprobeerd specifiek in te spelen op de eisen van de doelgroep en de opdrachtgevers. Het was even lastig om structuur aan te brengen in alle regels en verschillende aspecten van het huidige concept, maar uiteindelijk is dit gelukt. 
