---
title: Blog nr.15
subtitle: Vrijdag
date: 2018-06-01
categories: ["Design Challenge 4"]
---

Eindexpositie, Terugblik Design Challenge 4
<!--more-->


## Eindexpositie
Vandaag vond de eindexpositie van Design Challenge 4 plaats. Onze stand met het spel “Buurt aan de Beurt’ was een succes. Het is een spel waarbij wij willen dat jongeren uit de wijk Carnisse in beweging komen door samen te werken in de gehele wijk. Op deze manier zijn ze sociaal en actief bezig wat bijdraagt aan een prettige leefomgeving.

Om onze expo-stand op te laten vallen hebben we echte stoeptegels van het dakterras afgehaald om voor onze stand neer te leggen. Hierdoor krijg je als bezoeker echt het gevoel dat je op staat bent. Het stimuleerde de beleving dus op een leuke manier. 

De posters gemaakt door Gabi, Nick, Wesley en ikzelf gaven een goed beeld van het concept. We hebben afgewisseld in het presenteren bij de stand. We hebben erg veel positieve reacties gekregen op ons ontwerp en idee. Aan het einde van de dag hebben we zelfs te horen gekregen dat ons concept tot de beste acht behoorde!

## Terugblik Design Challenge 4

Ontwikkeling team:
Ik ben erg tevreden over de samenwerking in mijn huidige team. We luisteren goed naar elkaar en geven elkaar de ruimte waar dat nodig is. We nemen elkaars feedback serieus en zijn allemaal belust op een mooi resultaat. Natuurlijk zijn er ook nog genoeg verbeterpunten als team. We moeten in de toekomst nog beter ons ontwerpproces inrichten. Aan sommige dingen hadden we meer aandacht moeten besteden. Al met al kijk ik terug op een mooie samenwerking.

Persoonlijke ontwikkeling:
Ik merk dat ik erg vooruit gegaan ben. Ik ben enthousiast over de opleiding en de stof. Ik ga heel anders te werk ten opzichte van eerdere periodes. Aan het samenwerken ben ik meer gewend dan voorheen. Ik stel mijzelf vanaf het begin van een project al heel anders op dan toen ik aan deze opleiding begon. Het houden van overzicht blijft altijd lastig voor mij maar de puzzelstukjes van het ontwerpproces vallen steeds meer in elkaar. Ik snap waarom ik de taken die ik doe moet doen en ik doe deze naar behoren. Dit geeft me positieve energie.
