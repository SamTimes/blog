---
title: Blog nr.09
subtitle: Dinsdag
date: 2018-05-15
categories: ["Design Challenge 4"]
---

Brainstormsessie, Feedbacksessie
<!--more-->


## Brainstormsessie
Vandaag is er weer intensief gebrainstormd over het concept. We zijn er nog steeds niet uit wat ons definitieve concept precies gaat worden. Daarom hebben we besloten morgen af te spreken om onze brainstormsessie dan te vervolgen.

## Feedbacksessie
Feedback gevraagd aan docente Nikki Huisman over ons concept.
Ze zei als feedback dat we nog geen op zichzelf staand concept hebben. Volgens haar hebben we nu een interactief spel bedacht dat je nodig hebt bij het spelen van het spel. Het spel zou dan het concept zijn en het huisje het middel om het concept uit te kunnen voeren
