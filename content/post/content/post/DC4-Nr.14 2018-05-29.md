---
title: Blog nr.14
subtitle: Dinsdag
date: 2018-05-29
categories: ["Design Challenge 4"]
---

Peerfeedback besproken, Testrapport afgemaakt, Conceptposter
<!--more-->


## Peerfeedback besproken
Vandaag heb ik dan eindelijk met mijn teamgenoten de peerfeedback over elkaar besproken. Dit zijn altijd interessante en leerzame momenten en dat was het ook nu weer. Ik kreeg over het algemeen positieve feedback met hier en daar opbouwende kritiek. Ik was hier erg blij mee. Ook kreeg ik de nodige complimenten van mijn teamgenoten over de uitgebreide wijze waarop ik de peerfeedbackformulieren voor hun had ingevuld.

## Testrapport afgemaakt
Ik heb de nodige afbeeldingen toegevoegd aan het testrapport. Ook heb ik deze laten valideren door Robin van Wijk. De feedback die ik van hem kreeg op het testrapport heb ik gelijk doorgevoerd. De verbeterde versie van het testrapport heb ik zo snel mogelijk doorgespeeld aan mijn teamgenoten.

## Conceptposter
Het tijdstip was nu daar om een conceptposter te maken voor de aankomende expositie. Omdat mijn teamgenoten en ik bijna allemaal de competentie ‘Verbeelden en uitwerken’ nog moesten behalen, hebben we besloten om allemaal een eigen poster te maken.
