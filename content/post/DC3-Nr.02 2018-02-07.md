---
title: Blog nr.02
subtitle: Woensdag
date: 2018-02-07
categories: ["Design Challenge 3"]
---

De Rotterdam-opdracht Deel 1
<!--more-->

## De Rotterdam-opdracht Deel 1
Vandaag heb ik deelgenomen aan de vervangende Rotterdam-opdracht (alternatief voor de projectdagen in Maastricht. In verband met een keuzevak dat ik morgenochtend heb kon ik helaas niet deelnemen aan de projectdagen in Maastricht. Afwezigheid bij de eerste bijeenkomst van dit keuzevak zorgt voor uitsluiting van verdere deelname en er was geen mogelijkheid tot verschuiving.

We begonnen de dag met ongeveer 35-40 studenten. Het was de bedoeling dat we gezamenlijk zouden eten om zo met elkaar te socializen. Aangezien niemand van de studenten hier echt goed van op de hoogte was hadden de meesten al gegeten. Voor mij persoonlijk kwam dit moment goed uit. Het was jammer om te zien dat iedereen heel erg bij zijn/haar klasgenoten bleef zitten en men nog niet echt kennismaakte met studenten uit andere klassen.

De eerste opdracht was het maken van een persoonlijke avatar (bestaand uit een tekening, mijn teamrol en sterke en zwakke punten. Deze Avatars moesten we ophangen aan het bord. Verschillende docenten en peercoaches moesten aan de hand van hun vijf favoriete Avatars hun team samenstellen.

Toen ik in een team ingedeeld was hebben we eerst de opdracht besproken en daarna zijn we aan de slag gegaan. De bedoeling was dat we voor een bepaalde doelgroep en aspect van een levensstijl een oplossing of product moesten bedenken dat dit aspect van een levensstijl positief zou beïnvloeden. We moesten eerst een doelgroep kiezen. Ik en mijn teamgenoten hadden gekozen voor de doelgroep: ‘jongvolwassenen met kinderen’. Dit leek ons wel een uitdagende doelgroep omdat je hierbij ook met kinderen rekening moet houden. De verschillende richtingen waaruit we moesten kiezen waren voeding, beweging, ontspanning etc. We hebben uiteindelijk gekozen voor ontspanning omdat dit ons de minst voor de hand liggende richting leek.

Met mijn team heb ik vervolgens allerlei vragen opgesteld die we aan onze doelgroep wilden stellen om zoveel mogelijk te weten te komen over hen manieren en gewoonten wat ontspanning betreft en wat voor invloed dit heeft op hun levensstijl.

We zijn vervolgens de stad in gegaan maar we hadden nog niet erg veel resultaat geboekt. Toen de tijd al begon te dringen zijn we terug naar school gegaan waar we samen met de docenten, peercoaches en de overgebleven studenten evalueerde wat we gedaan hadden.

