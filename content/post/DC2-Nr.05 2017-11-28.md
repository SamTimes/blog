---
title: Blog nr.05
date: 2017-11-28
categories: ["Design Challenge 2"]
---

Vorige week hadden mijn teamgenoten en ik afgesproken om het afgelopen weekend allemaal wat mensen te interviewen die binnen onze doelgroep vallen. Dit was niemand echt gelukt omdat we geen van allen echt veel mensen kennen die binnen onze doelgroep vallen. Het loopt nog niet echt allemaal op rolletjes in mijn team heb ik het gevoel. We zijn deze dag niet echt veel vooruitgekomen. Iedereen was het wel met me eens dat we eigenlijk nog te weinig onderzoek gedaan hadden maar niemand wist echt wat we eraan moesten doen. Omdat we ook al volgens de planning allerlei opdrachten zoals de ‘Customer Journey Map’ enz. af moesten hebben was het hele team eigenlijk al een stap verder dan we moesten zijn. ’s Avonds hebben we gezamenlijk afgesproken om morgen buiten de normale projectmomenten om af te spreken en het nodige af te maken.