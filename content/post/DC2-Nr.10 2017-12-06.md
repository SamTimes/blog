---
title: Blog nr.10
date: 2017-12-06
categories: ["Design Challenge 2"]
---

’s Ochtends heeft mijn teamgenoot Thomas de pitch gehouden voor een aantal leraren en iemand van ontwerpersbureau Fabrique. We kregen als belangrijkste feedback dat we niet te veel moesten afwijken van de hoofdgedachte van de opdracht: het bevorderen van het herhaalbezoek. Ik was zelf ook al van mening dat dit het geval was maar dit werd vandaag dus bevestigd. Ook vond de opdrachtgever van Fabrique dat het idee van een app een te grote barrière is. Mensen vinden het installeren, begrijpen en gebruiken volgens hem vaak te veel moeite. Een app is bovendien veel effectiever als er al een opgebouwde relatie is met de gebruiker zei hij. Dit was natuurlijk erg nuttige feedback allemaal. Wel was de feedback over het algemeen positief. Onze ideeën werden wel als interessante ideeën ervaren. Ik heb achteraf de zojuist verkregen feedback met mijn teamgenoten besproken en we hebben gelijk actief nagedacht over hoe we ons huidige concept zouden gaan aanpassen naar aanleiding hiervan. 