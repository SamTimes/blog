---
title: Blog nr.18
date: 2018-01-16
categories: ["Design Challenge 2"]
---

Vandaag ben ik weer begonnen met het plannen van de dag samen met mijn teamgenoten. We hebben vandaag ons High Fidelity prototype getest. Dit heb ik samen met de andere jongens uit mijn groepje gedaan. Mijn rol was die van de ‘stille toeschouwer’. Dit hield in dat ik de testpersonen observeerde terwijl zij de door Sem gestelde vragen beantwoorden en de prototypes met eigen handen testten. Mijn bevindingen heb ik genoteerd en achteraf besproken met mijn teamgenoten. Hier kwamen nuttige dingen uit naar voren. We hebben het testen in twee verschillende tests verdeeld: 1: het onderdeel de ‘Paardpas en de Paardpaal’ en 2: het onderdeel de ‘Throwback-en suggestiemail’. We hebben beide onderdelen met een verschillend testpersoon getest. Later op de dag heb ik aan mijn leerdossier gewerkt.