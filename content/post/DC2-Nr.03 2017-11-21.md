---
title: Blog nr.03
date: 2017-11-21
categories: ["Design Challenge 2"]
---

Vandaag heb ik met mijn team de moeite genomen om elkaar nog wat beter te leren kennen. Zo hebben we bijvoorbeeld al onze SWOT-analyses doorgenomen. We hebben ook besproken wat ieders kwartaaldoelen zijn en dit genoteerd. Later heb ik het opzetje voor mijn leerdossier alvast gemaakt.