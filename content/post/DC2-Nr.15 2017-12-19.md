---
title: Blog nr.15
date: 2017-12-19
categories: ["Design Challenge 2"]
---

Vandaag ben ik met mijn groepje (op initiatief van captain Jeremy) begonnen met een rondje vertellen hoe het met iedereen gaat. Hierna heb ik samen met mijn groepsgenoten bekeken wat er deze week gedaan moet worden en de taken verdeeld. Het plan was dat ik samen met Sem aan de ‘Design Rationale’ zou gaan werken. We wilden ons eerst goed verdiepen in wat volgens ons een Design Rationale precies is om daarna allebei apart aan de slag te gaan met een verschillend deel hiervan. Voor mijn keuzevak ‘Creatief Denken’ heb ik mijn teamgenoten tegen het einde van de dag een aantal ‘Waarom-vragen’ gesteld. Dit moest ik doen om te leren goed door te vragen en wat de meerwaarde hiervan kan zijn. Van de Design Rationale was nog niet veel terecht gekomen. Ik heb toen samen met Sem afgesproken om de instructie hiervoor thuis nog door te lezen. 