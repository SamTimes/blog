---
title: Blog nr.09
subtitle: Vrijdag
date: 2018-03-09
categories: ["Design Challenge 3"]
---

Lifestyle Diary, Workshop Projectplanning
<!--more-->


## Lifestyle Diary
Wederom aan de Lifestyle Diary gewerkt. Deze voor moest voor 00:00 op N@Tschool ingeleverd worden. Ik het beroepsproduct op tijd ingeleverd.

## Workshop Projectplanning
Later op de dag heb ik de Workshop Projectplanning gevolgd. De presentaties tijdens deze workshop waren kort maar krachtig. Hier was ik blij mee. Hier heb ik van Margot de Heide, een docente met veel beroepspraktijkervaring geleerd hoe je het best kan plannen en wat verschillende projectmanagementmethodes zijn (bijv. Scrum, lean, waterfall etc.)
