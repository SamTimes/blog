---
title: Blog nr.12
date: 2017-12-12
categories: ["Design Challenge 2"]
---

Vandaag heb ik met mijn teamgenoten unaniem besloten om niet te gaan. Dit had vooral te maken met persoonlijke omstandigheden en verslechtte reisomstandigheden. Wel hebben we afgesproken om onze “verloren dag” morgen in te halen.