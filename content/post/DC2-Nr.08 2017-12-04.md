---
title: Blog nr.08
date: 2017-12-04
categories: ["Design Challenge 2"]
---

Aanvankelijk had ik samen met mijn team het idee om vandaag de stad (Rotterdam) in te gaan en in het centrum mensen die in onze doelgroep vallen te zoeken en te interviewen. Toen we na het hoorcollege met elkaar afspraken zijn we tot de conclusie gekomen dat het er niet als een zinvol idee meer uit zag. Wel hebben we afspraken gemaakt over hoe we onze ontwerpkeuzes toch op relevante informatie gaan baseren.