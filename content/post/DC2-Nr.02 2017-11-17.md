---
title: Blog nr.02
date: 2017-11-17
categories: ["Design Challenge 2"]
---

De eerste studiodag van het 2e kwartaal. We zijn klassikaal begonnen met het vormen van nieuwe teams. Dit moest aan de hand van de uitkomst van de eerder uitgevoerde ‘Belbin test’. Het was de bedoeling dat je stickers op je borst plakte met de twee meest dominante uitkomsten van de test. Aan de hand hiervan moesten we zelf groepjes samenstellen die zo divers mogelijk waren en dus zoveel mogelijk verschillende uitkomsten bij elkaar hadden. Ik vond dit zelf niet echt lekker werken omdat het vrij chaotisch verliep. Mensen gaan toch onbewust snel samen met iemand waar hun voorkeur naar uitgaat en er bleven steeds groepjes van 2 over. Ik zie zelf sowieso niet echt de meerwaarde van het zelf groepjes samenstellen. Wat mij betreft word je gewoon door de leraren ingedeeld in groepjes met mensen waar je nog niet eerder mee samen hebt gewerkt. Je moet toch met iedereen leren samen te werken en iedereen uit je klas komt toch weleens aan de beurt. 

Mijn nieuwe groepje komt rustig en vertrouwd over. Er wordt goed naar elkaar geluisterd merk ik. We hebben kennis met elkaar gemaakt door één voor één een verhaaltje over onszelf te houden. Dit op initiatief van groepsgenoot Sem die tijdens deze kennismaking belangrijke punten vastlegde in Word. Teamgenoot Jeremy stelde voor dat hij teamcaptain wilde zijn en dit werd door iedereen goedgekeurd.

Tot slot hebben we een aantal regels opgesteld waar we ons als team aan moeten houden dit kwartaal. Ook hebben we besproken hoe we te werk zullen gaan en via welke media we zullen gaan communiceren met elkaar. Zo gaan we bijvoorbeeld plannen via de website ‘Trello’ en delen we bestanden met elkaar via Google Drive. Dit vind ik zelf erg fijn omdat ik het gevoel had dat er gelijk duidelijkheid heerste en er goede afspraken gemaakt werden.
