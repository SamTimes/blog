---
title: Blog nr.21
date: 2017-10-24
categories: ["Design Challenge 1"]
---

Vandaag heb ik alle vijf soorten spelkaarten (kennisvragen, raadsels, pictionary, spreekwoorden, persoonlijke vragen) getekend in Sketch (voor+achterkanten). Dit was nog aardig wat werk waar ik me flink vergist in had. De volgende keer zorg ik dat ik alles al eerder gemaakt heb en niet nog de dag van tevoren tot laat moet zitten om het af te maken.