---
title: Blog nr.17
date: 2017-10-10
categories: ["Design Challenge 1"]
---

Met de verkregen feedback van vorige week aan de slag gegaan. Er waren vandaag door omstandigheden geen docenten voor onze Design Challenge uren aanwezig terwijl we juist een briefing zouden krijgen. Later heb ik samen met mijn klas nog wel uitleg gekregen over de Starrs en het leerdossier door andere docenten.