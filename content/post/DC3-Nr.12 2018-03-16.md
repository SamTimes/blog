---
title: Blog nr.12
subtitle: Vrijdag
date: 2018-03-16
categories: ["Design Challenge 3"]
---

Tussentijdse presentatie, Verwerken feedback, Taken verdelen, Planning aanpassen
<!--more-->


## Tussentijdse presentatie
’s Ochtends vond de tussentijdse presentatie plaats. Ik moest met mijn team aan een docente en een team uit de parallelklas duidelijk maken welke inzichten we tot nu toe verzameld hadden n.a.v. de huidige en de toekomstige situatie. Ook moesten we de ontwerpcriteria presenteren en de vervolgstappen toelichten. Ik was met mijn groepje als eerst aan de beurt om 9:30. Mijn taak was het presenteren van de toekomstige situatie d.m.v. de toekomstige Lifestyle Diary. Gezamenlijk hebben we verteld naar welke onderwerpen we onderzoek hebben gedaan en welke ontwerpcriteria wij belangrijk vinden om aan te houden. Teamcaptain Ricardio heeft geluidsopnames gemaakt van onze presentatie. Na de presentatie hebben we feedback gehad van het luisterende team en de docente. Van het andere team kreeg ik redelijk positieve feedback op mijn presenteren. Verbeterpunten waren dat ik nog enthousiaster over moest komen en meer iedereen aan moest kijken. Van de docente kregen wij als belangrijkste feedback dat we onze doelgroep moesten verkleinen, onze ontwerpcriteria veel specifieker moesten en ons doel, visie duidelijker moesten uitleggen.

## Verwerken feedback
’s Middags heb ik de feedback van de docent en medestudenten verwerkt samen met mijn teamgenoten. We hebben naar aanleiding van deze feedback onze doelgroep sterk verkleind. Mede op mijn initiatief tijdens de discussie die volgde kwamen we op de doelgroep: Studenten aan de Hogeschool Rotterdam met psychische problemen/klachten. We besloten dat het eerst goed moet zitten in je hoofd wil je denken aan je gezondheid en een gezonde levensstijl. Vanuit deze gedachte zijn we verder gegaan met deze nieuwe doelgroep.

## Taken verdelen
De taken zijn direct na het verwerken van de feedback verdeeld. Mijn taak voor het aanstaande weekend is om één of meerdere mensen met psychische problemen te interviewen om achter het verband tussen deze problemen en hun levensstijl te komen.

## Planning aanpassen
De planning werd gelijk hierna opnieuw doorgenomen. Ik heb deze nieuwe taken vervolgens ingevoerd op Trello.
