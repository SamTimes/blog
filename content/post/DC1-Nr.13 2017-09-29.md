---
title: Blog nr.13
date: 2017-09-29
categories: ["Design Challenge 1"]
---

Ik heb de feedback van de leraren goed doorgenomen

Ik heb het onderzoek van de Enquête doelgroep CMD van afgelopen dinsdag verder verwerkt. Ik heb de meest voorkomende antwoorden geselecteerd en hiermee een lijst samengesteld waar we op terug kunnen vallen tijdens het bedenken van het spel.

Aan het einde van de dag heb ik een aantal schetsen gemaakt van enkele ideeën voor een bord voor ons spel.
