---
title: Blog nr.05
date: 2017-09-08
categories: ["Design Challenge 1"]
---

Niet echt veel verder gekomen vandaag. Vooral heel erg gezocht naar inspiratie voor het spel. Leerzaam hoe je erachter komt dat samenwerken niet altijd even makkelijk is. Ik merk dat in ons groepje iedereen de samenwerking met elkaar soms vindt tegenvallen. Wel merk ik dat als we het hierover hebben en eerlijk tegen elkaar zijn we wel moeite voor elkaar doen. Toch wel een leerzame dag dus.