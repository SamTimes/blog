---
title: Blog nr.11
date: 2017-12-08
categories: ["Design Challenge 2"]
---

Vandaag heb ik met mijn team eerst weer gezamenlijk de taken verdeeld. Het plannen op de website ‘Trello’ verloopt soepel en iedereen houdt zich hierdoor dus aan de planning. Ik heb gewerkt aan nieuwe prototypes met in het achterhoofd de feedback en nieuwe ideeën van gisteren.