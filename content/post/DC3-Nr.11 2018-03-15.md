---
title: Blog nr.11
subtitle: Woensdag
date: 2018-03-15
categories: ["Design Challenge 3"]
---

Toekomstige Lifestyle Diary
<!--more-->


## Toekomstige Lifestyle Diary
Vandaag ben ik speciaal naar school gegaan om aan de toekomstige Lifestyle Diary te werken. Gabi had haar deel hiervan al gedaan en dit in de doos op school voor mij achtergelaten. Ik heb flink wat uurtjes gespendeerd aan het zorgvuldig uitkiezen van de indeling en plaatjes en het knippen en plakken hiervan. Met het resultaat was ik tevreden. Het vertegenwoordigt mooi de beoogde toekomstige levensstijl die wij als team met elkaar delen. Deze Lifestyle Diary kan morgen tijdens de presentatie gebruikt worden.
