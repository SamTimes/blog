---
title: Blog nr.13
subtitle: Dinsdag
date: 2018-03-20
categories: ["Design Challenge 3"]
---

Teambespreking, Workshop Creatieve Technieken Deel 1
<!--more-->


## Teambespreking
Ik heb in het afgelopen weekend informatie verzameld over onze nieuwe doelgroep. Dit onderzoek hebben we (samen met het onderzoek dat Gabi heeft gedaan) besproken. Ik heb twee interviews afgenomen bij mensen uit mijn omgeving die binnen de doelgroep vallen. Zij wilden liever anoniem blijven maar hebben erg open verteld over hun situatie. Tijdens deze interviews heb ik geprobeerd te achterhalen wat de correlaties tussen een gezonde levensstijl en psychische problemen (als depressie, burn-out etc.) zijn. Daarnaast wilde ik meer weten over wat de relatie van deze problemen is tot de thuissituatie/opvoeding.

De onderzoeksresultaten gaven een mooie indicatie van de worsteling waar iemand met deze problemen mee te maken heeft. Er komt duidelijk naar voren dat de studenten zich nauwelijks met hun levensstijl bezig houden omdat ze weinig eigenwaarde hebben of zelfs zelfdestructief zijn. Deze informatie heb ik doorgespeeld aan mijn teamgenoten. Zij reageerden positief op deze uitkomsten.

Teamcaptain Ricardio had op mijn verzoek een eigen template gemaakt voor de peerfeedbackformulieren. Deze heeft hij aan ons uitgedeeld. Wij denken dat het waardevoller is dat we elkaar feedback geven apart van elkaar, dus zonder dat je oordeel beïnvloed wordt door dat van de anderen die het formulier al voor jou hebben ingevuld. Ieder heeft nu dus een formulier uitgedeeld gekregen waarop je namens jezelf al je teamgenoten voorziet van feedback.



## Workshop Creatieve Technieken Deel 1
Omdat ik graag meer wilde leren over creatieve technieken die ik zou kunnen gebruiken heb ik vandaag de workshop Creatieve Technieken Deel 1 gevolgd. Het was een erg leerzame workshop. We begonnen met een hele grappige ‘Energizer’. We moesten namelijk een portret tekenen van een andere deelnemer terwijl je deze aan moest blijven kijken. Je mocht niet naar je blaadje kijken en je pen/potlood niet loslaten. De bedoeling was dat we hierdoor wat losser werden en met vernieuwde energie aan de workshop zouden beginnen.
Na de nodige uitleg over hoe je een ‘creatieve brainstormsessie’ mogelijk zou kunnen voorbereiden, moest iedereen voor zichzelf gaan bedenken hoe zijn/haar voorbereide brainstormsessie eruit zou komen te zien. Hierbij kon je gebruik maken van verschillende boeken en spelkaarten met creatieve technieken erop. Als er een teamgenoot deelnam aan dezelfde workshop mocht je samenwerken. Aangezien ik wist dat mijn teamgenote Gabi dezelfde workshop op een ander tijdstip volgde heb ik haar later die dag gecontacteerd met de vraag of ze wilde samenwerken. Dit wilde ze graag. We hebben afgesproken om a.s. woensdag in onze vrije tijd de workshop op school voor te bereiden.
