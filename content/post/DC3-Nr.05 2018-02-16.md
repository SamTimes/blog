---
title: Blog nr.05
subtitle: Vrijdag
date: 2018-02-16
categories: ["Design Challenge 3"]
---

Presentatie plan van aanpak, Feedback verwerken
<!--more-->

## Presentatie plan van aanpak
Vandaag heeft de presentatie van het plan van aanpak van mijn team plaatsgevonden. In dit plan van aanpak hebben we voor iedereen een eigen spelkaart gemaakt met zijn/haar eigen sterke en zwakke punten. Hierbij moest iedereen zijn/haar eigen levensstijl ook in het kort beschrijven. Hiernaast was mijn taak het presenteren van onze ‘teamkaart’. Hierop stonden onze gezamenlijke doelen en onze gezamenlijke krachten beschreven. Hiernaast heb ik uitgelegd waar wij als team voor staan en wat ons gezamenlijke doel is. De presentatie verliep erg gesmeerd. Iedereen vertelde netjes zijn/haar stukje en er werd steeds enthousiast gereageerd door de twee docenten.

## Feedback verwerken
Onze presentatie viel erg goed in de smaak bij de docenten. Ze vonden het zelfs veelbelovend. Ze vonden het erg goed dat we het vraagstuk wat de levensstijl betreft zo op onszelf betrokken hadden. 

Feedback op het plan van aanpak:
-	Leg nog duidelijker is het belangrijk dat de jongeren uit Rotterdam-Zuid een gezondere levensstijl aannemen? 
-	Ontwerpvragen moesten concreter en geen onderzoeksvragen zijn
-	Er zijn meer stakeholders dan de stakeholders die we hebben aangegeven
-	Uren inplannen in de planning, en hierop terug reflecteren.

Na de presentatie hebben we gezamenlijk de feedback verwerkt. We hebben de ontwerpvragen en onderzoeksvragen concreter te maken. Hierna zijn gelijk de nieuwe taken binnen het team verdeeld.
