---
title: Blog nr.08
date: 2017-09-15
categories: ["Design Challenge 1"]
---

Nog een aantal moodboards gemaakt. Eerst over de doelgoep (CMD studenten), later over ons huidige thema (kleurrijk, cultureel Rotterdam). Zeker bij de laatste van die 2 kreeg ik zelf de nodige inspiratie en besefte ik me hoe kleurrijk de stad Rotterdam wel niet is op vele verschillende manieren.