---
title: Blog nr.03
date: 2017-09-05
categories: ["Design Challenge 1"]
---

Ik ben samen met mijn teamgenoten helemaal teruggegaan naar de achterliggende gedachte van de opdracht. We kwamen erachter dat we de opdracht niet helemaal op dezelfde manier hadden geïnterpreteerd. Zo had ik bijvoorbeeld het idee dat de belangrijkste doelen van de opdracht waren dat de eerstejaars studenten elkaar leerden kennen en de stad Rotterdam. Dit bracht mij al snel in de richting van een spel over Rotterdam waar je elkaar leert kennen terwijl je het speelt. Mijn vier groepsgenoten maakten uit de opdracht op dat het elkaar leren kennen centraal stond en Rotterdam leren kennen minder belangrijk was. Na hier een tijdje goed over gediscussieerd te heb ik me bij hun standpunt aangesloten. Ik besefte me dat het een belangrijk onderdeel is van samenwerken dat je je soms over je eigen ideeën of meningen heen moet zetten als anderen waar je mee samen werkt er niet zo over denken. 