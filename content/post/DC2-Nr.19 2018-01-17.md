---
title: Blog nr.19
date: 2018-01-17
categories: ["Design Challenge 2"]
---

De eindexpositie van deze Design Challenge heeft vandaag alweer plaatsgevonden! Het afgelopen kwartaal is voor mijn gevoel erg snel gegaan. Toen de afgezanten van ontwerpbureau Fabrique (de opdrachtgever) langs kwamen was ik net begonnen met het bezichtigen van de concepten van de andere groepjes. Wel kwam ik er snel achter dus heb ik het grootste gedeelte inclusief de feedback meegekregen. De feedback was positief maar ik kreeg niet de indruk dat het duo echt overtuigd was. Leuk was het feit dat ze mijn ‘Paardpas en de brief leuk vonden. Ze vonden dat dit bevorderend was voor de beleving omdat mensen dan iets tastbaars hebben. Dit was leuk om te horen.