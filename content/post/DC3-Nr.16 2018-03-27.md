---
title: Blog nr.16
subtitle: Dinsdag
date: 2018-03-27
categories: ["Design Challenge 3"]
---

Workshop Creatieve Technieken Deel 2
<!--more-->


## Workshop Creatieve Technieken Deel 2
Vandaag stond deel 2 van de workshop Creatieve Technieken op de planning. Dit was eigenlijk een validatiemoment. Hiervoor heb ik in samenwerking met Gabi een terugblik/reflectie verwerkt in een verslag. We hebben hierin op onszelf en elkaar gereflecteerd. Docente Mieke was erg tevreden over onze inzet en manier van werken. Ze merkte dat we de opdracht erg serieus genomen hebben en dat we er veel bruikbare informatie door hebben gewonnen. Het beste vond ze om te zien dat wij nu de meerwaarde van een creatieve brainstormsessie in zagen en vooral de nauwkeurige voorbereiding hiervan. De opdracht is succesvol gevalideerd voor Gabi en ik. 
