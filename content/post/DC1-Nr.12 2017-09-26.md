---
title: Blog nr.12
date: 2017-09-26
categories: ["Design Challenge 1"]
---

Ik heb de enquête uitgedeeld aan onze mede CMD-studenten. Ongeveer 100 exemplaren in totaal. Over het algemeen reageerde men enthousiast op de open vragen en waren ze snel beantwoord en weer opgehaald. Enkelen hadden er geen zin in en zagen het als teveel moeite om het A4tje in te vullen. Het koste me uiteindelijk nog aardig wat tijd en moeite voor ik de meeste van de blaadjes weer ingevuld terug had.

Ik heb opnieuw flink lang gebrainstormd. Met mijn groepje heb ik via het maken van een COCD BOX bepaalde knopen kunnen doorhakken en een beter beeld kunnen vormen bij wat het nieuwe concept moet worden. (deze techniek hebben we geleerd tijdens het afgelopen werkcollege)

Einde van de dag goed gesprek gehad met mijn groepsgenoten en later ook met een leraar er bij. We kwamen tot de conclusie dat de samenwerking tot nu toe nog niet helemaal verloopt zoals het zou moeten. We hebben gepraat over hoe goed we elkaar eigenlijk kennen. Kennen we elkaars professionele ik?
