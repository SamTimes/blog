---
title: Blog nr.17
subtitle: Vrijdag
date: 2018-03-30
categories: ["Design Challenge 3"]
---

Goede vrijdag, Leerdossier inleveren, Terugblik Design Challenge 3
<!--more-->


## Goede vrijdag
Vandaag waren er geen studiolessen of workshops i.v.m. Goede Vrijdag.

## Leerdossier inleveren
Het individuele leerdossier van onderwijsperiode 3 moest vandaag ingeleverd worden. Het tijdstip van de deadline: 17:00. Ik heb mijn leerdossier op tijd ingeleverd.

## Terugblik Design Challenge 3

#### Ontwikkeling team:
Ik ben erg tevreden over de samenwerking in mijn huidige team. We luisteren goed naar elkaar en geven elkaar de ruimte waar dat nodig is. We nemen elkaars feedback serieus en zijn allemaal belust op een mooi resultaat. Natuurlijk zijn er ook nog genoeg verbeterpunten als team. We moeten in de toekomst nog beter ons ontwerpproces inrichten. Aan sommige dingen hadden we meer aandacht moeten besteden. Dit heeft te maken met het indelen van de tijd die we ervoor hebben. We zijn iets te laat echt op gang gekomen en dit heeft geresulteerd in een naar mijn mening niet heel veel zeggend Low Fidelity Prototype. We zijn dus al met al goed op weg maar er is nog genoeg werk aan de winkel. Een mooie uitdaging voor de volgende periode waarin ik weer verder ga met ditzelfde team. Ik ben benieuwd of we de samenwerking en het resultaat daardoor ook tot een nieuw en hoger niveau kunnen brengen.

#### Persoonlijke ontwikkeling:
Ik merk dat ik stappen aan het maken ben. Ik ga heel anders te werk ten opzichte van eerdere periodes. Aan het samenwerken ben ik meer gewend dan voorheen. Ik stel mijzelf vanaf het begin van een project al heel anders op dan toen ik aan deze opleiding begon. Het houden van overzicht blijft altijd lastig voor mij maar ik de puzzelstukjes van het ontwerpproces vallen steeds meer in elkaar. Ik snap waarom ik de taken die ik moet doen moet doen en ik doe deze naar behoren. Ik ga met vernieuwde energie en frisse moed aan het volgende kwartaal beginnen en ik hoop weer een stap dichterbij het niveau te komen dat aan het einde van dit schooljaar van mij verwacht wordt.

