---
title: Blog nr.16
date: 2018-01-09
categories: ["Design Challenge 2"]
---

De kerstvakantie is afgelopen. Eerst hebben mijn teamgenoten en ik de tijd genomen om aan elkaar te vertellen hoe onze kerstvakanties geweest waren en daarna zijn we weer aan de slag gegaan. Ik heb mijn deel van de Design Rationale afgemaakt. Ik schrok een beetje toen ik hoorde dat volgende week vrijdag het leerdossier af moest zijn en dat de woensdag daarvoor de expo al is. Ik heb aangegeven aan mijn teamgenoten dat ik graag van nut wil zijn bij het maken van ons High Fidelity Prototype. Later op de dag heb ik de workshop ‘User Testing’ gevolgd. Hier werd uitgelegd hoe je het testplan voor het ‘High Fidelity Prototype’ zo zorgvuldig en nuttig mogelijk kan opstellen. Ik vond de workshop erg nuttig omdat ik de achterliggende gedachte achter/het nut van het testplan nu veel beter begrijp.