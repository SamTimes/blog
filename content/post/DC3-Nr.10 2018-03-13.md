---
title: Blog nr.10
subtitle: Dinsdag
date: 2018-03-13
categories: ["Design Challenge 3"]
---

Feedbackmoment Lifestyle Diaries, Brainstormen concept, Taken verdelen
<!--more-->

## Feedbackmoment Lifestyle Diaries
Ik heb met mijn teamgenoten een feedbackmoment ingelast voor onze Lifestyle Diaries. Tijdens dit moment hebben we één voor één de tijd genomen om de Lifestyle Diaries van de anderen te beoordelen. Hierna heb ik ook feedback gevraagd aan een docente. Naar aanleiding hiervan heb ik nog een aantal kleine wijzigingen aangebracht.

## Brainstormen concept
Later op de dag hebben we als team gebrainstormd over eventuele ideeën/concepten. Aangezien mij teamrol aan het begin van het project ‘Concepter’ was zou het voor de hand liggend geweest zijn dat ik hier het voortouw in zou nemen. Dit was niet nodig. We hebben gezamenlijk als team middels verschillende creatieve technieken gebrainstormd over ons concept. Hierbij kreeg iedereen evenveel ruimte om zijn/haar ideeën te uiten en het was niet nodig dat er één iemand de leiding nam.

## Taken verdelen
Tot slot zijn gezamenlijk de taken verdeeld. Mijn taak is samen met teamgenote Gabi het verzorgen van de Toekomstige Lifestyle Diary. Ook hebben we besproken wie wat zou voorbereiden voor de tussentijdse presentatie van aanstaande vrijdag. 

