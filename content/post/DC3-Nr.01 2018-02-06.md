---
title: Blog nr.01
subtitle: Dinsdag
date: 2018-02-06
categories: ["Design Challenge 3"]
---

Start/Kick-off Design Challenge 3, Praktijkvraagstuk/debriefing
<!--more-->


## Start/Kick-off Design Challenge 3
Vandaag heeft de kick-off van het nieuwe project plaatsgevonden. Hierbij kregen we voorlichting van zowel docenten als mensen uit de praktijk. Vanuit het Expertisecentrum Maatschappelijke Innovatie (EMI) en de gemeente Rotterdam in samenwerking met de Hogeschool Rotterdam hebben wij een opdracht gekregen waarin wij te maken krijgen met ‘Wicked Problems’. Uit onderzoek is gebleken dat er in de regio Rotterdam Zuid gemiddeld een ongezondere levensstijl onder de mensen heerst ten opzichte van andere wijken in Rotterdam. Vooral de jongeren hebben de prioriteit van de opdrachtgevers. De mensen uit Rotterdam Zuid beoordelen hun eigen levensstijl dan ook negatiever dan waar dan ook in Rotterdam. 

Voor dit probleem moeten wij met innovatieve oplossingen komen die ervoor kunnen zorgen dat de bewoners van Rotterdam Zuid gezonder gaan leven. Een ongezonde levensstijl kan natuurlijk met veel verschillende aspecten te maken hebben. Voorbeelden hiervan zijn; armoede, weinig sportieve activiteiten, slechte voeding etc. Hoe dan ook is de taak aan ons om met een innovatieve oplossing of meerdere oplossingen te komen om hier verbetering in te krijgen.

## Praktijkvraagstuk/debriefing
Na afloop heb ik de verkregen informatie voor mijzelf uitgewerkt in een praktijkvraagstuk/debriefing. Hierin heb ik het vraagstuk samengevat, nagedacht over wat de ontwerpvragen zijn bij dit project en de opdrachtgevers, stakeholders en de doelgroep genoteerd.


![2018-02-06](afbeelding1.jpg)
