---
title: Blog nr.15
subtitle: Vrijdag
date: 2018-03-23
categories: ["Design Challenge 3"]
---

Creatieve Brainstormsessie, Knopen doorhakken Concept, Peerfeedback bespreken, Taken verdelen, Begin Low Fidelity Prototype
<!--more-->


## Creatieve Brainstormsessie
Een drukke dag. We zijn uiteraard begonnen met de door Gabi en ik geplande brainstormsessie. In het kort hebben we toegelicht wat we wilde bereiken en welke technieken er aan de pas zouden komen. Via WhatsApp hadden we de dag van tevoren al foto’s van de spelkaarten met de gekozen creatieve technieken erop toegestuurd. Dan kon ieder teamlid alvast bekijken wat de bedoeling was.

We hebben een leeg lokaal geregeld waar we de hele dag konden zitten. Alle benodigde materialen hebben we uitgestald op de tafel en ervoor gezorgd dat elk tweetal het plan van aanpak voor zich had liggen. Ik ben de sessie gestart door het plan van aanpak uit te leggen en te wijzen op de regels en afspraken. Het gehele team ging hiermee akkoord.

Vervolgens heb ik de kennismakingsopdracht: ‘teken elkaar naar terwijl je elkaar aankijkt’ uitgelegd. De opdracht was snel duidelijk en viel goed in de smaak bij de anderen. Zeer geslaagd.

Toen volgde de opdrachtentechnieken Mindmapping, Negatief Brainstormen, Ideeën ontleden en Nieuw, nodig, uitvoerbaar. Ik vond zelf vooral de laatste techniek erg bruikbaar. Hier gingen we in op de mate van innovativiteit, de wenselijkheid en de haalbaarheid van onze concepten. Deze opdracht was wat moeilijker op gang te brengen. Het had er o.a. meet te maken dat het de laatste van de brainstormsessie was maar ook gewoon omdat de opdracht wat lastiger is. Toen er nog onduidelijkheid heerste bij een aantal teamgenoten over wat precies de bedoeling was heb ik het nog een keer uitgelegd maar deze keer heb ik het wat anders verwoord. Hierna viel het kwartje al snel en konden we verder aan de slag. Het was de bedoeling dat we op de mate van innovativiteit, de wenselijkheid en de haalbaarheid van verschillende concepten/ideeën in zouden gaan. Dit hebben we ook gedaan maar dan op verschillende aspecten van een aantal van onze concepten/ideeën. Dit maakte het iets ingewikkelder maar het zorgde er wel voor dat we al een stapje verder waren in ons creatieve denkproces. Uiteindelijk was iedereen erg positief over de manier waarop de opdracht was gebracht en over de uitkomst ervan. Ik ben blij dat iedereen tot het einde toe gemotiveerd is gebleven en actief meegedaan heeft.


## Knopen doorhakken Concept
Door de creatieve brainstormsessie hebben mijn teamgenoten en ik de nodige beslissingen en keuzes kunnen maken over ons concept. Zo hebben we bijvoorbeeld besloten wat het voorlopig definitieve concept gaat worden en hoe dit concept eruit komt te zien.

## Peerfeedback bespreken
Na een welverdiende pauze hebben mijn teamgenoten en ik elkaar de van tevoren ingevulde peerfeedback laten zien om dit vervolgens één voor één, per persoon toe te lichten. Er werd erg open gesproken. Iedereen luisterde goed en iedereen nam zijn/haar feedback serieus. Ik ben over het algemeen tevreden over mijn feedback. Sommige verbeterpunten waren misschien wel eyeopeners voor mij. Ik heb deze punten verder uitgewerkt in mijn peerfeedbackverslag.

## Taken verdelen
Vervolgens zijn de taken binnen het team verdeeld. Omdat er niet echt veel taken meer over zijn naast het maken van het Low Fidelity Prototype, is het samen met twee andere teamgenoten mijn taak om dit te verzorgen.

## Begin Low Fidelity Prototype
Het maken van het Low Fidelity Prototype verliep een beetje chaotisch. We waren er snel uit hoe we het gingen doen (namelijk middels een nagebootste mobiel met schermen) maar daarna werden er niet echt afspraken gemaakt over wie wat ging maken. Aangezien we moeilijk met zijn drieën aan dezelfde dingen konden knippen/knutselen kan ik niet zeggen dat ik er echt veel aan gedaan heb. Wel heb ik een aantal schermen bedacht en getekend. Het prototype was vrij snel klaar.
