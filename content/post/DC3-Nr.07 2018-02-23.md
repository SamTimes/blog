---
title: Blog nr.07
subtitle: Vrijdag
date: 2018-02-23
categories: ["Design Challenge 3"]
---

Workshop Interview voorbereiden
<!--more-->


## Workshop Interview voorbereiden
Tot slot heb ik vandaag de workshop Interview voorbereiden gevolgd. Dit leek me erg waardevol aangezien ik nog veel te leren heb op dit gebied. Tijdens de workshop heb ik actief meegewerkt en nuttige dingen opgestoken. Ik heb geleerd hoe belangrijk een goede voorbereiding is. Dit begint bij het goed bedenken wat je te weten wilt komen van de persoon die je interviewt. Ook is het van belang dat je goed bepaald waar het interview gaat plaatsvinden, hoe laat etc. Ik ga deze informatie zeker gebruiken als ik in het vervolg weer onderzoek ga doen.
