---
title: Blog nr.16
date: 2017-10-04
categories: ["Design Challenge 1"]
---

Vandaag heb ik samen met Nick ons concept voor iteratie 2 gepresenteerd.
Dit was een grote overwinning vooral voor mezelf omdat ik een enorme hekel heb aan presenteren. Ik heb toch mijn verantwoordelijkheid genomen omdat naast Nick mijn andere groepsgenoten er ook totaal geen zin in hadden. De presentatie ging goed! We hebben samen de inleiding gedaan (hoofdthema, subthema’s, doelgroep en doel van het spel). Daarna begon Nick met het vertellen hoe we tot het huidige concept zijn gekomen, wat het spelconcept is en hoe het prototype en het online element eruit zien. Hierna heb ik de spelregels uitgebreid uitgelegd en vervolgens wat onze conclusies waren na de eerder uitgevoerde spelanalyse. Later heb ik verteld over de tot nu toe verkregen feedback op ons pel en wat volgens ons naar aanleiding van deze feedback verbeterpunten zijn voor ons spel. Tot slot heb ik samen met Nick verschillende vragen beantwoord. Vooral van de 3 kritische ‘juryleden’. Zij gaven gelijk al scherpe feedback die ik later op de dag op papier van ze ontvangen heb.
