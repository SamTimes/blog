---
title: Blog nr.03
subtitle: Donderdag
date: 2018-02-08
categories: ["Design Challenge 3"]
---

De Rotterdam-opdracht Deel 2
<!--more-->


## De Rotterdam-opdracht Deel 2
Vandaag heeft het tweede deel van de vervangende Rotterdam-opdracht plaatsgevonden. Er was al snel duidelijk dat een groot gedeelte van de groep van de dag ervoor het af had laten weten. Na uitleg over wat er op de planning stond zijn we begonnen. Hierdoor zijn de overgebleven mensen opnieuw ingedeeld in groepjes. Doordat het grootste deel van mijn groepje er weer was, hebben wij op onze bevindingen van gisteren voortgeborduurd.

Ik ben samen met mijn groepje uiteindelijk tot het concept gekomen van een multifunctionele Kubus die op allerlei manieren voor ontspanning kan zorgen. We noemden het ‘The Relaxing Cube’. De Kubus kon zogenaamd door een app bediend worden. De opties; allerlei kleuren, geuren en geluiden verspreiden door de ruimte die de ontspannende sfeer stimuleren.

Uiteindelijk was het de bedoeling dat dit concept werd uitgewerkt in een Paper Prototype. Dit hebben we gedaan door heel slim een mobiel met zaklamp in een papieren kubus te verwerken. Tijdens de presentatie die we aan het eind van de dag moesten geven heb ik met mijn team er zoveel mogelijk aan gedaan om de beleving zo goed mogelijk over te brengen. We hebben alle lichten in het lokaal uit gedaan, via het mobieltje onder de kubus het licht nagebootst terwijl deze ook zeer rustige, ontspannende muziek afspeelde. Als ‘finishing touch’ hebben we zelf met parfum in de lucht gespoten.

Ik vond de afgelopen twee dagen in zekere mate leerzaam. Wel heb ik het gevoel dat er te weinig met de tijd gedaan is. Het overgrote gedeelte van de medestudenten was zeer slecht gemotiveerd en is er gewoon mee gekapt. Ik voelde me hier toch een beetje lullig over. Het had blijkbaar geen consequenties als je gewoon weg ging. Uiteindelijk ben je hier voor jezelf dus met die gedachte plus de complimenten van de docenten en peercoaches heb ik toch een goed gevoel overgehouden aan de afgelopen twee dagen. Nu met frisse moed op naar het echte project!
